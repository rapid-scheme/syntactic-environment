;; Copyright (C) 2017 Marc Nieper-Wißkirchen

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

(define-library (rapid syntactic-environment-test)
  (export run-tests)
  (import (scheme base)
          (rapid test)
	  (rapid comparator)
	  (rapid syntax)
	  (rapid syntactic-environment))
  (begin
    (define (run-tests)
      (test-begin "Syntactic environments")

      (test-group "Aliases"
	(define env (make-syntactic-environment))
	(define alias (make-alias env 'foo))
	
	(test-assert "alias?"
	  (alias? alias))

	(test-assert "identifier?: alias"
	  (identifier? alias))

	(test-assert "identifier?: symbol"
	  (identifier? 'bar))

	(test-equal "identifier->symbol: alias"
	  'foo
	  (identifier->symbol alias))

	(test-equal "identifier->symbol: symbol"
	  'bar
	  (identifier->symbol 'bar))

	(test-assert "bound-identifier=?: true"
	  (bound-identifier=? alias alias))

	(test-assert "bound-identifier=?: false"
	  (not (bound-identifier=? alias 'foo))))

      (test-group "Denotations"
	(define denotation (make-denotation 'transformer 'location))

	(test-assert "denotation?"
	  (denotation? denotation))

	(test-equal "denotation-transformer"
	  'transformer
	  (denotation-transformer denotation))

	(test-equal "denotation-location"
	  'location
	  (denotation-location denotation)))
      
      (test-group "Bindings"
	(define syntax (datum->syntax #f 'foo))
	(define denotation (make-denotation 'transformer 'location))
	(define binding (make-binding syntax #f denotation))

	(test-assert "binding?"
	  (binding? binding))

	(test-equal "binding-syntax"
	  syntax
	  (binding-syntax binding))

	(test-equal "binding-denotation"
	  denotation
	  (binding-denotation binding))

	(test-equal "binding-immutable?"
	  #f
	  (binding-immutable? binding)))

      (test-group "Syntactic environments"
	(define env (make-syntactic-environment))
	(define mut (make-denotation 'transformer 'location))
	(define immut (make-denotation 'transformer 'location))
	(define id1 (syntax id1))
	(define id2 (syntax id2))
	(define id3 (syntax id3))
	(define alias (make-alias env 'id1))
	(define id4 (syntax ,alias))
	(parameterize ((current-syntactic-environment env))
	  (insert-binding! id1 mut)
	  (insert-binding! id2 immut #t))
	
	(test-assert "syntactic-environment?: make-syntactic-environment"
	  (syntactic-environment? env))

	(test-assert "syntactic-environment?: new-syntactic-environment"
	  (syntactic-environment?
	   (parameterize ((current-syntactic-environment env))
	     (new-syntactic-environment))))
	
	(test-equal "lookup-binding!"
	  mut
	  (binding-denotation
	   (parameterize ((current-syntactic-environment env))
	     (lookup-binding! id1))))

	(test-error "lookup-binding: not bound"
		    (parameterize ((current-syntactic-environment env))
		      (lookup-binding! id3)))

	(test-equal "lookup-binding/default!: not bound"
	  #f
	  (parameterize ((current-syntactic-environment env))
	    (lookup-binding/default! id3 #f)))
	
	(test-assert
	    "insert-binding!: can change meaning of identifiers in new scope"
	  (parameterize ((current-syntactic-environment env))
	    (parameterize ((current-syntactic-environment
			    (new-syntactic-environment)))
	      (insert-binding! id1 immut #t)
	      #t)))
	
	(test-error "insert-binding!: change meaning of fixed identifier"
		    (parameterize ((current-syntactic-environment env))
		      (parameterize ((current-syntactic-environment
				      (new-syntactic-environment)))
			(lookup-binding! id1)
			(insert-binding! id1 immut #t))))
	
	(test-equal "lookup-binding!: syntactic closures"
	  mut
	  (binding-denotation
	   (parameterize ((current-syntactic-environment
			   (make-syntactic-environment)))
	     (lookup-binding! id4))))

	(test-assert "maybe-isolate: isolate"
	  (parameterize ((current-syntactic-environment env))
	    (parameterize ((current-syntactic-environment
			    (new-syntactic-environment)))
	      (maybe-isolate
	       #t
	       (lambda ()
		 (lookup-binding! id1)))
	      (insert-binding! id1 immut #t)
	      #t)))

	(test-error "maybe-isolate: do not isolate"
		    (parameterize ((current-syntactic-environment env))
		      (parameterize ((current-syntactic-environment
				      (new-syntactic-environment)))
			(maybe-isolate
			 #f
			 (lambda ()
			   (lookup-binding! id1)))
			(insert-binding! id1 immut #t)
			#t)))

	(test-assert "identifier=?"
	  (identifier=? env (unwrap-syntax id1) env (unwrap-syntax id4)))
	
	(test-assert "free-identifier=?: true"
	  (parameterize ((current-syntactic-environment env))
	    (free-identifier=? (unwrap-syntax id1) (unwrap-syntax id4))))

	(test-assert "free-identifier=?: false"
	  (not
	   (parameterize ((current-syntactic-environment env))
	     (free-identifier=? (unwrap-syntax id1) (unwrap-syntax id2)))))

	(test-assert "bound-identifier=?: true"
	  (parameterize ((current-syntactic-environment env))
	    (bound-identifier=? (unwrap-syntax id1) (unwrap-syntax id1))))

	(test-assert "bound-identifier=?: false"
	  (not
	   (parameterize ((current-syntactic-environment env))
	     (bound-identifier=? (unwrap-syntax id1) (unwrap-syntax id4)))))

	(test-assert "make-free-identifier-comparator"
	  (comparator?
	   (parameterize ((current-syntactic-environment env))
	     (make-free-identifier-comparator))))

	(test-assert "make-free-identifier-comparator"
	  (parameterize ((current-syntactic-environment env))
	    (=? (make-free-identifier-comparator)
		(unwrap-syntax id1)
		(unwrap-syntax id4)))))
      
      (test-end))))
