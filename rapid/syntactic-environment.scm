;; Copyright (C) 2017 Marc Nieper-Wißkirchen

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Identifiers

(define-record-type <alias>
  (%make-alias environment identifier symbol identity)
  alias?
  (environment alias-environment)
  (identifier alias-identifier)
  (symbol alias-symbol)
  (identity alias-identity))

(define (make-alias environment identifier)
  (let ((identity (generate-identity)))
    (%make-alias environment identifier (identifier->symbol identifier) identity)))

(define (identifier? obj)
  (or (symbol? obj)
      (alias? obj)))

(define (identifier->symbol identifier)
  (if (symbol? identifier)
      identifier
      (alias-symbol identifier)))

(define (bound-identifier=? identifier1 identifier2)
  (eq? identifier1 identifier2))

;;; Denotations

(define-record-type <denotation>
  (%make-denotation transformer location identity)
  denotation?
  (transformer denotation-transformer)
  (location denotation-location)
  (identity denotation-identity))

(define (make-denotation transformer location)
  (let ((identity (generate-identity)))
    (%make-denotation transformer location identity)))

;;; Bindings

(define-record-type <binding>
  (make-binding syntax immutable? denotation)
  binding?
  (syntax binding-syntax)
  (immutable? binding-immutable?)
  (denotation binding-denotation))

;;; Syntactic environments

(define-record-type <syntactic-environment>
  (%make-syntactic-environment bindings fixed-identifiers)
  syntactic-environment?
  (bindings syntactic-environment-bindings syntactic-environment-set-bindings!)
  (fixed-identifiers fixed-identifiers set-fixed-identifiers!))

(define (make-syntactic-environment)
  (%make-syntactic-environment (mapping identifier-comparator)
			       (mapping identifier-comparator)))

(define (new-syntactic-environment)
  (%make-syntactic-environment (current-bindings)
			       (mapping identifier-comparator)))

(define current-fixed-identifiers
  (case-lambda
   (() (fixed-identifiers (current-syntactic-environment)))
   ((identifiers)
    (set-fixed-identifiers! (current-syntactic-environment) identifiers))))

(define (fix-identifier! identifier binding)
  (current-fixed-identifiers (mapping-set (current-fixed-identifiers)
					  identifier
					  binding)))

(define (release-identifier! identifier)
  (current-fixed-identifiers (mapping-delete (current-fixed-identifiers)
					     identifier)))

(define syntactic-environment-ref
  (case-lambda
    ((identifier)
     (syntactic-environment-ref (current-syntactic-environment) identifier #f))
    ((environment identifier)
     (syntactic-environment-ref environment identifier #t))
    ((environment identifier isolated?)
     (let loop ((environment environment)
		(id identifier))
       (cond
	((mapping-ref/default (syntactic-environment-bindings environment)
			      id
			      #f)
	 => (lambda (binding)
	      (unless isolated?
		(fix-identifier! identifier binding))
	      binding))
	(else
	 (cond
	  ((alias? id)
	   (let ((environment (alias-environment id))
		 (id (alias-identifier id)))
	     (loop environment id)))
	  (else
	   (unless isolated?
	     ;; The unboundedness of the identifier is going to be
	     ;; fix.
	     (fix-identifier! identifier #t))
	   #f))))))))

(define (lookup-binding!/default identifier-syntax default)
 (let ((identifier (unwrap-syntax identifier-syntax)))
   (or (syntactic-environment-ref identifier)
       (begin (release-identifier! identifier)
	      default))))

(define (lookup-binding! identifier-syntax)
  (let ((identifier (unwrap-syntax identifier-syntax)))
    (or (syntactic-environment-ref identifier)
	(raise-syntax-error identifier-syntax
			    "identifier ‘~a’ not bound"
			    (identifier->symbol identifier)))))

(define insert-binding!
  (case-lambda
    ((identifier-syntax denotation)
     (insert-binding! identifier-syntax denotation #f))
    ((identifier-syntax denotation immutable?)
     (let ((identifier (unwrap-syntax identifier-syntax)))
       (cond
	((and-let*
	     ((binding (mapping-ref/default (current-fixed-identifiers)
					    identifier
					    #f))
	      (or (eq? binding #t)
		  (not (eq? (binding-denotation binding) denotation))))
	   binding)
	 => (lambda (binding)
	      (raise-syntax-error identifier-syntax
				  "meaning of identifier ‘~a’ cannot be changed"
				  (identifier->symbol identifier))
	      (when (binding? binding)
		(raise-syntax-note (binding-syntax binding)
				   "identifier was bound here"))
	      #f))
	(else
	 (let ((binding
		(make-binding identifier-syntax
			      immutable?
			      denotation)))
	   (fix-identifier! identifier binding)
	   (current-bindings (mapping-set (current-bindings)
					  identifier
					  binding)))))))))

(define (maybe-isolate isolate? thunk)
  (if isolate?
      (let ((old-fixed-identifiers #f)
	    (new-fixed-identifiers (current-fixed-identifiers)))
	(dynamic-wind
	    (lambda ()
	      (set! old-fixed-identifiers (current-fixed-identifiers))
	      (current-fixed-identifiers new-fixed-identifiers))
	    thunk
	    (lambda ()
	      (set! new-fixed-identifiers (current-fixed-identifiers))
	      (current-fixed-identifiers old-fixed-identifiers))))
      (thunk)))

(define (identifier=? environment1 identifier1 environment2 identifier2)
  (let*
      ((binding1
	(syntactic-environment-ref environment1 identifier1))
       (binding2
	(syntactic-environment-ref environment2 identifier2))
       (denotation1
	(and binding1 (binding-denotation binding1)))
       (denotation2
	(and binding2 (binding-denotation binding2))))
    (cond
     ((and denotation1 denotation2)
      (eq? denotation1 denotation2))
     ((and (not denotation1) (not denotation2))
      (eq? (identifier->symbol identifier1)
	   (identifier->symbol identifier2)))
     (else
      #f))))

(define (free-identifier=? identifier1 identifier2)
  (identifier=? (current-syntactic-environment) identifier1
		(current-syntactic-environment) identifier2))

(define (make-free-identifier-comparator)
  
  (define environment (current-syntactic-environment))
  
  (define (free-identifier=? identifier1 identifier2)
    (identifier=? environment identifier1
		  environment identifier2))
  
  (define (free-identifier<? identifier1 identifier2)
    (let*
	((binding1
	  (syntactic-environment-ref environment identifier1 #t))
	 (binding2
	  (syntactic-environment-ref environment identifier2 #t))
	 (denotation1
	  (and binding1 (binding-denotation binding1)))
	 (denotation2
	  (and binding2 (binding-denotation binding2))))
      (cond
       ((and denotation1 denotation2)
	(< (denotation-identity denotation1)
	   (denotation-identity denotation2)))
       ((and (not denotation1) denotation2))
       (else
	(string<? (symbol->string (identifier->symbol identifier1))
		  (symbol->string (identifier->symbol identifier2)))))))
  
  (make-comparator identifier?
		   free-identifier=?
		   free-identifier<?
		   #f))

;;;; Parameter objects

(define current-syntactic-environment
  (make-parameter #f))

(define current-bindings
  (case-lambda
   (() (syntactic-environment-bindings (current-syntactic-environment)))
   ((bindings)
    (syntactic-environment-set-bindings! (current-syntactic-environment)
					 bindings))))

;;;; Comparators

(define (identifier-ordering-predicate i1 i2)
  (if (symbol? i1)
      (or (not (symbol? i2))
	  (string<? (symbol->string i1) (symbol->string i2)))
      (and (not (symbol? i2))
	   (< (alias-identity i1) (alias-identity i2)))))

(define (identifier-hash-function i)
  (if (symbol? i)
      (symbol-hash i)
      (alias-identity i)))

(define identifier-comparator
  (make-comparator identifier?
		   bound-identifier=?
		   identifier-ordering-predicate
		   identifier-hash-function))

(comparator-register-default! identifier-comparator)
