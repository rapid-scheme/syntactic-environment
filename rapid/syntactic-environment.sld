;; Copyright (C) 2017 Marc Nieper-Wißkirchen

;; This program is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;> Syntactic environments.

(define-library (rapid syntactic-environment)
  (export make-alias
          alias?
	  alias-environment
	  alias-identifier
          identifier?
	  identifier->symbol
	  identifier-comparator
	  bound-identifier=?
	  identifier=?
	  make-denotation denotation? denotation-location denotation-transformer
	  make-binding binding? binding-syntax binding-immutable? binding-denotation
	  make-syntactic-environment
	  syntactic-environment?
	  new-syntactic-environment
	  current-syntactic-environment
	  lookup-binding!
	  lookup-binding!/default
	  insert-binding!
	  maybe-isolate
	  free-identifier=?
	  make-free-identifier-comparator)
  (import (scheme base)
	  (scheme case-lambda)
	  (rapid and-let)
	  (rapid identity)
          (rapid comparator)
	  (rapid mapping)
	  (rapid syntax))
  (include "syntactic-environment.scm"))
